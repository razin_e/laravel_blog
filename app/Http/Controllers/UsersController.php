<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail;
use Illuminate\Http\Request;


class UsersController extends Controller
{

// показывает список пользователей
    public function index()
    {
        $data['users'] = User::all();
        return view('users/index', $data);
    }

// показывает страницу чата и форму отправки сообщений
    public function showChat(){
        $data['user'] = User::all();
        $data['mail'] = Mail::all();
        return view('users/show', $data);
    }

// добавление записи в базу данных
    public function addMessage($id){
        dd(request()->all());
/*        $this->validate(request(),[
            'name' => 'required|string', //длина максимум 30 символов
            'message' => 'required|string',
        ]);

        $post = User::findOrFail($id);
        $post->update(request()->all());*/
        return redirect('/users/mail/');
    }

// показывает подробную информацию(login, email) о пользователе
    public function about($id){
        $data['user'] = User::find($id);
        return view('users/description', $data);
    }

}