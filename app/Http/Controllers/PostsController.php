<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;


class PostsController extends Controller
{
    public function index()
    {
        $data['posts'] = Post::all();
        return view('index', $data);
    }

    public function show(Post $post){

        return view('posts/show', compact('post'));

    }

    public function create(){
        return view( 'posts/create');
    }

    public function store(){

        $this->validate(request(),[
            'title' => 'required|string', //input должен быть заполнен и должен быть string
            'alias' => 'required|unique:posts',
            'intro' => 'required|string',
            'body' => 'required|string',
        ]);

        Post::create(request()->all());
        return redirect('/');
    }

    public function edit(Post $post){

        return view('posts/edit', compact('post'));

    }

    public function update(Post $post){
        $this->validate(request(),[
            'title' => 'required|max:30', //длина максимум 30 символов
            'alias' => 'required',
            'intro' => 'required|string',
            'body' => 'required|string',
        ]);

        $post->update(request()->all());
        return redirect('/');
    }

    public function delete(Post $post){

        return view('posts/delete', compact('post'));
    }

    public function destroy(Post $post){

        $post->delete();
        return redirect('/');
    }
}
