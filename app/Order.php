<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['customer_name', 'phone', 'email', 'feedback'];

    public function products(){
        return $this->belongsToMany(Product::class);
    }


}
