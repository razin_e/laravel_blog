<?php
/* Добавляю выборку юзеров */
Route::get('/users', 'UsersController@index');
/*в папку Http/Controllers добавляю UsersController с методом index*/

Route::get('/users/mail/', 'UsersController@showChat');

Route::post('users/mail/', 'UsersController@addMessage');

Route::get('/users/description/{user}', 'UsersController@about');

Route::get('/', 'PostsController@index');

Route::get('/posts/create', 'PostsController@create');

Route::get('/posts/{post}', 'PostsController@show');

Route::post('/posts', 'PostsController@store');

Route::get('/posts/{post}/edit','PostsController@edit');

Route::put('/posts/{post}', 'PostsController@update');

Route::get('/posts/{post}/delete', 'PostsController@delete');

Route::delete('/posts/{post}', 'PostsController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/posts/{post}/comment', 'CommentsController@store');

Route::get('/products', 'ProductsController@index');

Route::get('/products/{product}','ProductsController@show');

Route::get('/cart/{product}', 'CartsController@store');

Route::get('/order', 'OrdersController@create');

Route::post('/order', 'OrdersController@store');