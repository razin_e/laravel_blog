@extends('template')

@section('content')

<div class="container">
    <div class="row">
        @foreach($posts as $post)

            <div class="col-md-4">
                <h2> {{$post->title}} </h2>
                <p> {{$post->intro}} </p>
                <a href="/posts/{{$post->alias}}" class="btn btn-default">Ream more</a>
                <a href="/posts/{{$post->alias}}/edit" class="btn btn-warning" >Edit</a>
                <a href="/posts/{{$post->alias}}/delete" class="btn btn-danger"> Delete </a>

            </div>
        @endforeach
    </div>
</div>

@endsection