
@extends('template')

@section('content')
<div class="container">
    <div class="from-database">
        <p style="text-align: center"><i> общий чат </i></p>
        @foreach($mail as $mails)
                <b>{{ $mails -> user_name }}</b>
                <p>{{ $mails-> message }}</p>
            <hr>
            @endforeach
        </div>

        <div class="form-group">
         <p>
             {{ Auth::user()->name }}
         </p>
        </div>

        <div class="form">
            <form action="/users/mail/" method="POST">

                {{csrf_field()}}

                <div class="form-group">
                    <input type="hidden" name="name" value="{{ Auth::user()->name }}">
                </div>
                <div class="form-group">
                    <textarea name="text" name="message" class="form-control"> </textarea>
                </div>

                <div class="form-group">
                    <input type="submit" name="send" value="send" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@endsection