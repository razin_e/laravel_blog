@extends('template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Edit post:</h2>

            <form action="/posts/{{$post->alias}}" method="POST">

                {{method_field('PUT')}}  <!-- то же что -> <input type="hidden" name="_method" value="PUT">-->
                {{csrf_field()}}

                <div class="form-group">
                    <label for="title">Title: </label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
                </div>

                <div class="form-group">
                    <label for="alias">Alias: </label>
                    <input type="text" class="form-control" id="alias" name="alias" value="{{$post->alias}}">
                </div>

                <div class="form-group">
                    <label for="intro">Intro: </label>
                    <textarea name="intro" id="intro" class="form-control" >{{$post->intro}}</textarea>
                </div>

                <div class="form-group">
                    <label for="body">Body: </label>
                    <textarea name="body" id="body" class="form-control" >{{$post->body}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary"> save </button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection