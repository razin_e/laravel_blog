@extends('template')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create new post:</h2>

                    <form action="/posts" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Title: </label>
                            <input type="text" class="form-control" id="title" name="title" >
                        </div>

                        <div class="form-group">
                            <label for="Alias"> Alias: </label>
                            <input type="text" class="form-control" id="alias" name="alias" >
                        </div>

                        <div class="form-group">
                            <label for="intro">Intro: </label>
                            <textarea name="intro" id="intro" class="form-control" ></textarea>
                        </div>

                        <div class="form-group">
                            <label for="body">Body: </label>
                            <textarea name="body" id="body" class="form-control" ></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary"> save </button>
                        </div>

                        @include('layout/errors')
                    </form>
            </div>
        </div>
    </div>
@endsection