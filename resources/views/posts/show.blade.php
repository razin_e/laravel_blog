@extends('template')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-offset-2">
            <h1> {{$post->title}} </h1>
            <p> {{$post->intro}} </p>
            <hr>
            <p> {{$post->body}}</p>
        </div>
     <div class="col-md-8 col-offset-2">
         <ul>
             @foreach($post->comments as $comment)
             <li>{{$comment->body}}</li>
             @endforeach
         </ul>
     </div>

        <div class="col-md-8 col-offset-2">
            <form action="/posts/{{$post->alias}}/comment" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="body">Your comment</label>
                    <input type="text" name="body" id="body" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Add comment</button>
                </div>
            </form>
        </div>

    </div>
</div>

@endsection