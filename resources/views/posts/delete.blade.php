@extends('template')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-offset-2">
            <h1> Do you want to delete {{$post->title}} ?</h1>

                <form action="/posts/{{$post->alias}}" method="post">

                    {{method_field('DELETE')}}
                    {{csrf_field()}}

                    <div class="form-control">
                        <button class="btn btn-danger"> DELETE </button>
                    </div>
                </form>

        </div>
    </div>
</div>

@endsection