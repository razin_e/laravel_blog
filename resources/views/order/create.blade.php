@extends('template')

@section('content')
    <div class="col-md-8">
        @foreach($products as $product)
            <h2>{{$product->title}} x {{$cart[$product->id]}}</h2>
        @endforeach

        <form action="/order" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="customer_name"> Enter your name: </label>
                <input type="text" name="customer_name" id="customer_name" class="form-control">
            </div>

            <div class="form-group">
                <label for="phone"> Enter your phone: </label>
                <input type="text" name="phone" id="phone" class="form-control">
            </div>

            <div class="form-group">
                <label for="email"> Enter your email: </label>
                <input type="text" name="email" id="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="feedback"> Enter your feedback: </label>
                <textarea name="feedback" id="feedback" class="form-control"> </textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Create order</button>
            </div>
        </form>
    </div>
@endsection