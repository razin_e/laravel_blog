@extends('template')

@section('content')

<div class="container">
    <div class="row">
        @foreach($products as $product)

        <div class="col-md-4">

            <h2> {{$product->title}} </h2>
            <p>
                Price: {{$product->price}}
            </p>

            <a href="/products/{{$product->alias}}" class="btn btn-default">Ream more</a>

        </div>
        @endforeach
    </div>
</div>

@endsection