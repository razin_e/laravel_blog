<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'PHP is awesome',
                'alias' => 'PHP_is_awesome',
                'intro' => 'Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. 
                Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв 
                шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила 
                п\'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною' ,
                'body' => 'PHP is awesomePHP is awesomePHP is awesomePHP is awesomePHP is awesome',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'title' => 'Javascript is boo',
                'alias' => 'Javascript_is_boo',
                'intro' => 'Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. 
                Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв 
                шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила 
                п\'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною' ,
                'body' => 'PHP is awesomePHP is awesomePHP is awesomePHP is awesomePHP is awesome',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'title' => 'I need jQuery',
                'alias' => 'I_need_jQuery',
                'intro' => 'Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. 
                Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв 
                шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила 
                п\'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною' ,
                'body' => 'PHP is awesomePHP is awesomePHP is awesomePHP is awesomePHP is awesome',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
